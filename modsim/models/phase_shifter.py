import numpy as np

def linear(Ein, Vin, Vpi = 3.5):

    return Ein * np.exp(1j * (Vin / Vpi) * np.pi)

def heater(Ein, Vin, Vpi = 1):

    # quadratic phase shift from heating
    return Ein * np.exp(1j * ((Vin / Vpi) ** 2) * np.pi)
