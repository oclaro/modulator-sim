import numpy as np
import modsim.models.phase_shifter as ps
import modsim.models.mach_zehnder as mzm

def linear(Ein, Vin_I, Vin_Q, Vbias_I, Vbias_Q, Vbias_P, 
    Vpi_rf_I = 3.5, Vpi_rf_Q = 3.5, Vpi_bias_I = 3.5, Vpi_bias_Q = 3.5, 
    Vpi_bias_P = 3.5, ER_model = 'Infinite', ER_dB_I = 20, ER_dB_Q = 20,
    ER_dB_P = 20, get_arms = False):

    if ER_model == 'Infinite':

        # equal splitting
        Ein_I = Ein / 2
        Ein_Q = Ein / 2

    elif ER_model == 'Finite':

        # get split factor from ER in dB
        ER = 10 ** (ER_dB_P / 20)
        k = 0.5 - 0.5 * (1 / ER)

        Ein_I = Ein * k
        Ein_Q = Ein * (1 - k)

    else:
        raise ValueError('Invalid value for model : ' + str(ER_model))

    Eout_I = mzm.lin_push_pull(Ein_I, Vin_I, Vbias_I, Vpi_rf = Vpi_rf_I, 
        Vpi_bias = Vpi_bias_I, ER_model = ER_model, ER_dB = ER_dB_I)

    Eout_Q = mzm.lin_push_pull(Ein_Q, Vin_Q, Vbias_Q, Vpi_rf = Vpi_rf_Q, 
        Vpi_bias = Vpi_bias_Q, ER_model = ER_model, ER_dB = ER_dB_Q)

    Eout_Q = ps.linear(Eout_Q, Vbias_P, Vpi = Vpi_bias_P)

    Eout = Eout_I + Eout_Q

    if get_arms:
        return [Eout, Eout_I, Eout_Q]
    else:
        return Eout

def lin_with_heaters(Ein, Vin_I_1, Vin_I_2, Vin_Q_1, Vin_Q_2,
    Vbias_I_1, Vbias_I_2, Vbias_Q_1, Vbias_Q_2, Vbias_P_1, Vbias_P_2,
    Vpi_rf_I = 3.5, Vpi_rf_Q = 3.5, Vpi_bias_I = 1, Vpi_bias_Q = 1, 
    Vpi_bias_P = 1, ER_model = 'Infinite', ER_dB_I = 20, ER_dB_Q = 20,
    ER_dB_P = 20, get_arms = False):

    if ER_model == 'Infinite':

        # equal splitting
        Ein_I = Ein / 2
        Ein_Q = Ein / 2

    elif ER_model == 'Finite':

        # get split factor from ER in dB
        ER = 10 ** (ER_dB_P / 20)
        k = 0.5 - 0.5 * (1 / ER)

        Ein_I = Ein * k
        Ein_Q = Ein * (1 - k)

    else:
        raise ValueError('Invalid value for model : ' + str(ER_model))

    Eout_I = mzm.lin_with_heaters(Ein_I, Vin_I_1, Vin_I_2, 
        Vbias_I_1, Vbias_I_2, Vpi_rf_1 = Vpi_rf_I, Vpi_rf_2 = Vpi_rf_I, 
        Vpi_bias_1 = Vpi_bias_I, Vpi_bias_2 = Vpi_bias_I, 
        ER_model = ER_model, ER_dB = ER_dB_I)

    Eout_Q = mzm.lin_with_heaters(Ein_Q, Vin_Q_1, Vin_Q_2, 
        Vbias_Q_1, Vbias_Q_2, Vpi_rf_1 = Vpi_rf_Q, Vpi_rf_2 = Vpi_rf_Q, 
        Vpi_bias_1 = Vpi_bias_Q, Vpi_bias_2 = Vpi_bias_Q, 
        ER_model = ER_model, ER_dB = ER_dB_Q)

    Eout_I = ps.heater(Eout_I, Vbias_P_1, Vpi = Vpi_bias_P)
    Eout_Q = ps.heater(Eout_Q, Vbias_P_2, Vpi = Vpi_bias_P)

    Eout = Eout_I + Eout_Q

    if get_arms:
        return [Eout, Eout_I, Eout_Q]
    else:
        return Eout



