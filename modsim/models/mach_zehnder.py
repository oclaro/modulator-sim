import numpy as np
import modsim.models.phase_shifter as ps

def lin_push_pull(Ein, Vrf, Vbias, Vpi_rf = 3.5, 
    Vpi_bias = 3.5, ER_model = 'Infinite', ER_dB = 20):

    phase = (Vrf / (2 * Vpi_rf)) * np.pi + (Vbias / (2 * Vpi_bias)) * np.pi

    if ER_model == 'Infinite':

        Eout = Ein * np.cos(phase)

    elif ER_model == 'Finite':

        ER_lin = 10 ** (ER_dB / 10)
        Eout = Ein * (np.cos(phase) + 1j * (1 / np.sqrt(ER_lin)) * np.sin(phase))

    else:
        raise ValueError('Invalid value for model : ' + str(ER_model))

    return Eout

def lin_with_heaters(Ein, Vrf_1, Vrf_2, Vbias_1, Vbias_2, 
    Vpi_rf_1 = 3.5, Vpi_rf_2 = 3.5, Vpi_bias_1 = 1, Vpi_bias_2 = 1, 
    ER_model = 'Infinite', ER_dB = 20):

    if ER_model == 'Infinite':

        # equal splitting
        E1_a = Ein / 2
        E2_a = Ein / 2

    elif ER_model == 'Finite':

        # get split factor from ER in dB
        ER = 10 ** (ER_dB / 20)
        k = 0.5 - 0.5 * (1 / ER)

        E1_a = Ein * k
        E2_a = Ein * (1 - k)

    else:
        raise ValueError('Invalid value for model : ' + str(ER_model))

    # phase shift of arm 1
    E1_b = ps.linear(E1_a, Vrf_1, Vpi = Vpi_rf_1)
    E1_c = ps.heater(E1_b, Vbias_1, Vpi = Vpi_bias_1)

    # phase shift of arm 2
    E2_b = ps.linear(E2_a, Vrf_2, Vpi = Vpi_rf_2)
    E2_c = ps.heater(E2_b, Vbias_2, Vpi = Vpi_bias_2)

    # combining fields from both arms
    Eout = E1_c + E2_c

    return Eout