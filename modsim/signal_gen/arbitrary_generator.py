import numpy as np
from scipy import signal
import os

class SignalGen:

	def __init__(self, num_of_samples = 1024, sampling_time = 0.01, samples_per_symbol = 2):

		self.N = num_of_samples
		self.Ts = sampling_time
		self.time = np.arange(0, self.N) * self.Ts
		self.frequency = np.linspace(-0.5, 0.5, len(self.time)) * (1 / self.Ts)
		self.sps = samples_per_symbol
		self.N_bits = int(self.N / self.sps)

	def waveform_gen(self, amp, freq, phase, waveform = 'sine', duty_cycle = 0.5):

		if waveform == 'sine':

			out = amp * np.sin(2 * np.pi * freq * self.time + phase)

		elif waveform == 'cosine':

			out = amp * np.cos(2 * np.pi * freq * self.time + phase)

		elif waveform == 'square':

			out = amp * signal.square(2 * np.pi * freq * self.time, duty = duty_cycle)

		else:
			raise ValueError('Invalid value for waveform : ' + str(waveform))

		return out

	def pattern_gen(self):

		bit_seq = [int(x) for x in (np.random.normal(size = self.N_bits) > 0)]

		return np.array(bit_seq)

	def gaussian_filter(self, data_in, bw, order):

		# defining gaussian filter
		G_f = np.exp(- np.log(np.sqrt(2)) * (abs(self.frequency) / bw) ** (2 * order))

		# converting <data_in> to frequency domain
		data_in_f = np.fft.fftshift(np.fft.fft(data_in))
		data_out_f = G_f * data_in_f
		data_out = np.fft.ifft(np.fft.ifftshift(data_out_f))

		return data_out

	def pulse_sequence_gen(self, pulse_type, amp, f_bw = None, f_order = None):

		if pulse_type == 'NRZ':

			bits = self.pattern_gen()

			out = 2 * bits - 1
			out = amp * (np.repeat(out, self.sps))

		elif pulse_type == 'PAM4':

			# lane 0
			bits_0 = self.pattern_gen()
			out_0 = 2 * bits_0 - 1
			out_0 = np.repeat(out_0, self.sps)

			# lane 1
			bits_1 = self.pattern_gen()
			out_1 = 2 * bits_1 - 1
			out_1 = np.repeat(out_1, self.sps)

			# PAM4
			out = 2 * out_0 - out_1
			out = amp * (out / out.max())

		else:
			raise ValueError('Invalid value for pulse type : ' + str(pulse_type))

		# apply gaussian amplitude filtering if desired 
		if f_bw is not None:	
			if f_order is None:
				f_order = 1

			out = self.gaussian_filter(out, bw = f_bw, order = f_order)

		return out





