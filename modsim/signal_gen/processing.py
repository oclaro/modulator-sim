import numpy as np
from scipy import signal

def get_dc_ac(data):
    ''' Returns DC and AC components of input signal '''

    # get dc level
    dc = np.mean(data)

    # get ac level
    ac = data - dc

    return (dc, ac)

def gaussian_filter(data_in, f0, bw, order, Ts):

    frequency = np.linspace(-0.5, 0.5, data_in.size) * (1 / Ts)

    # defining gaussian filter
    G_f = np.exp(-np.log(np.sqrt(2)) * ((abs(frequency) - f0) / bw) ** (2 * order))

    # converting <data_in> to frequency domain
    data_in_f = np.fft.fftshift(np.fft.fft(data_in))
    data_out_f = G_f * data_in_f
    data_out = np.fft.ifft(np.fft.ifftshift(data_out_f))

    return data_out

def cross_correlation(data_in, freq, Ts, N = None, delay = 0):

    # defining size
    if N is None or (N > data_in.size):
        N_ = data_in.size
    else:
        N_ = N


    # time domain parameters
    time = np.arange(0, N_) * Ts
    phase = 2 * np.pi * freq * time + delay

    # base of orthogonal sinusoids
    lut_exp = np.exp(1j * phase)

    # cross-correlation integrals
    return np.mean(lut_exp * data_in[:N_])





