from setuptools import setup, find_packages

setup(
    name = "modsim",
    version = "1.0",
    packages = find_packages(),
    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
    },

    # metadata for upload
    author = "Vinicius Cruz",
    author_email = "vinicius.cruz@idea-ip.com",
    description = "Modulator simulator for bias control study",
    license = "MIT",
    keywords = "IQ modulator bias control simulator",
)